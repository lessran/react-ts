// const arr = [12,3,4,5,6,7,9,98,4,5,63,2,11,2,4,8,96,2,48,5,4,5,85,7,9,78,9,65,413,1,65,54,65,4,5,2,7,9,6,4,5,87,76,1,3,45,9,7,95]

// const result = arr.reduce((acc, cur) => {
//     if (cur in acc) {
//         acc[cur]++;
//     } else {
//         acc[cur] = 1;
//     }
//     return acc;
// },{})
// console.log(result);

const compose = (...fn) => (...args) => (fn.reduce((a,b) => a(b(...args))));

const fn1 = (a,b) => (a+b);
const fn2 = (a) => (a*2);

console.log(compose(fn2,fn1)(1,2));

